#pragma once
#include <Windows.h>

#define CCMD_PROMPT "CCMD> "
#define CCMD_MAX_COMMAND_LEN 1024

typedef BOOL(WINAPI* CCMD_COMMAND_HANDLER)(LPWSTR*, INT, LPVOID);


typedef struct _CCMD_COMMAND {
	LPWSTR Command;
	LPWSTR Help;
	CCMD_COMMAND_HANDLER Handler;
} CCMD_COMMAND, *PCCMD_COMMAND;


VOID CommandLoop(PCCMD_COMMAND Commands, DWORD CommandCount, LPVOID Context);