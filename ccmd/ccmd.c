#include <Windows.h>
#include <stdio.h>

#include "ccmd.h"


BOOL CcmdReadLine(LPWSTR** ArgvOut, INT* ArgcOut);
PCCMD_COMMAND CcmdGetCommand(PCCMD_COMMAND Commands, DWORD CommandCount, PWCHAR CommandString);
VOID CcmdPrintUsage(PCCMD_COMMAND Commands, DWORD CommandCount);


VOID 
CommandLoop(PCCMD_COMMAND Commands, DWORD CommandCount, LPVOID Context)
{
	BOOL Run = TRUE;

	while (Run) {
		PCCMD_COMMAND Command = NULL;
		LPWSTR* Argv = NULL;
		INT Argc = 0;

		Run = CcmdReadLine(&Argv, &Argc);
		if (!Run) {
			goto loop_clean;
		}

		Command = CcmdGetCommand(Commands, CommandCount, Argv[0]);
		if (NULL == Command) {
			CcmdPrintUsage(Commands, CommandCount);

			Run = TRUE;
			goto loop_clean;
		}


		Run = Command->Handler(Argv, Argc, Context);

	loop_clean:
		if (Argv) LocalFree(Argv);
	}

	return;
}


BOOL
CcmdReadLine(LPWSTR** ArgvOut, INT* ArgcOut)
{
	BOOL Return = FALSE;
	DWORD BytesRead = 0;
	INT RetVal = 0;
	DWORD CommandBuffIndex = 0;
	CHAR CommandBuff[CCMD_MAX_COMMAND_LEN + 1] = { 0 };
	WCHAR WideCommandBuff[CCMD_MAX_COMMAND_LEN + 1] = { 0 };
	LPWSTR* Argv = NULL;
	INT Argc = 0;

	Return = WriteFile(GetStdHandle(STD_OUTPUT_HANDLE), CCMD_PROMPT, lstrlenA(CCMD_PROMPT), &BytesRead, NULL);
	if (!Return) {
		goto cleanup;
	}

	while (TRUE) {
		CHAR CurrChar = 0;

		Return = ReadFile(GetStdHandle(STD_INPUT_HANDLE), &CurrChar, 1, &BytesRead, NULL);
		if (!Return) {
			goto cleanup;
		}

		// Read until we hit \r\n, need to read that \n out of the buffer as well
		if ('\r' == CurrChar) {
			Return = ReadFile(GetStdHandle(STD_INPUT_HANDLE), &CurrChar, 1, &BytesRead, NULL);
			if (!Return) {
				goto cleanup;
			}

			break;
		}

		// Only store data up to the max size of our buffer
		if (CCMD_MAX_COMMAND_LEN > CommandBuffIndex) {
			CommandBuff[CommandBuffIndex] = CurrChar;
			CommandBuffIndex++;
		}
	}

	RetVal = MultiByteToWideChar(CP_ACP, 0, CommandBuff, lstrlenA(CommandBuff), WideCommandBuff, CCMD_MAX_COMMAND_LEN);
	if (lstrlenA(CommandBuff) != RetVal) {
		goto cleanup;
	}

	// THIS NEEDS TO BE FREED WITH LOCAL FREE
	Argv = CommandLineToArgvW(WideCommandBuff, &Argc);
	if (NULL == Argv) {
		goto cleanup;
	}

	*ArgvOut = Argv;
	*ArgcOut = Argc;

	Argv = NULL;
	Return = TRUE;

cleanup:
	if (Argv) LocalFree(Argv);
	return Return;
}


PCCMD_COMMAND 
CcmdGetCommand(PCCMD_COMMAND Commands, DWORD CommandCount, PWCHAR CommandString)
{
	PCCMD_COMMAND RetCmd = NULL;

	for (DWORD i = 0; i < CommandCount; i++) {
		if (0 == _wcsnicmp(Commands[i].Command, CommandString, wcslen(Commands[i].Command))) {
			RetCmd = &Commands[i];
			goto cleanup;
		}
	}

cleanup:
	return RetCmd;
}


VOID 
CcmdPrintUsage(PCCMD_COMMAND Commands, DWORD CommandCount)
{
	fprintf(stdout, "Usage: \n");
	for (DWORD i = 0; i < CommandCount; i++) {
		fprintf(stdout, "\t%ws %ws\n", Commands[i].Command, Commands[i].Help);
	}
}