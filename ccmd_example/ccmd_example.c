#include <Windows.h>
#include <stdio.h>

#include "../ccmd/ccmd.h"

BOOL WINAPI ExitHandler(LPWSTR *Argv, INT Argc, LPVOID Ctx);
BOOL WINAPI TestHandler(LPWSTR* Argv, INT Argc, LPVOID Ctx);

CCMD_COMMAND Commands[] = {
	{ L"exit", L"", ExitHandler },
	{ L"test", L"this is the help", TestHandler }
};

INT
main(INT argc, PCHAR argv[])
{
	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

	CommandLoop(Commands, _countof(Commands), NULL);
	return 0;
}


BOOL WINAPI 
ExitHandler(LPWSTR* Argv, INT Argc, LPVOID Ctx)
{
	UNREFERENCED_PARAMETER(Argv);
	UNREFERENCED_PARAMETER(Argc);
	UNREFERENCED_PARAMETER(Ctx);

	return FALSE;
}


BOOL WINAPI 
TestHandler(LPWSTR* Argv, INT Argc, LPVOID Ctx)
{
	UNREFERENCED_PARAMETER(Argv);
	UNREFERENCED_PARAMETER(Argc);
	UNREFERENCED_PARAMETER(Ctx);

	fprintf(stdout, "Lets keep the loop going\n");
	return TRUE;
}